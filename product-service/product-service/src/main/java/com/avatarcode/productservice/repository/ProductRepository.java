package com.avatarcode.productservice.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.avatarcode.productservice.model.Product;

@Repository
public interface ProductRepository  extends MongoRepository<Product ,String>{

}
