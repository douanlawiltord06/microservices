package com.avatarcode.productservice.services;

import java.util.List;

import org.springframework.stereotype.Service;

import com.avatarcode.productservice.dto.ProductRequest;
import com.avatarcode.productservice.dto.ProductResponse;
import com.avatarcode.productservice.model.Product;
import com.avatarcode.productservice.repository.ProductRepository;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@RequiredArgsConstructor
@Slf4j
public class ProductService {
    
    private final ProductRepository productRepository;

    public void createProduct(ProductRequest productRequest){

        Product product =  Product.builder()
        .name(productRequest.getName())
        .description(productRequest.getDescription())
        .price(productRequest.getPrice())
        .build();

        productRepository.save(product);
        log.info("product {} is save "+product.getId());
    }



    public List<ProductResponse> getAllProducts() {
          List<Product> products= productRepository.findAll();
          return   products.stream().map(this::mapToResponse).toList();
    }



    public ProductResponse mapToResponse(Product product){
        return ProductResponse.builder()
                .id(product.getId())
                .name(product.getName())
                .description(product.getDescription())
                .price(product.getPrice())
                .build();
    }
}
