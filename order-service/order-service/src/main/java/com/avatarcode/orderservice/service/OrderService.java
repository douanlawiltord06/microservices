package com.avatarcode.orderservice.service;

import java.util.List;
import java.util.UUID;

import org.springframework.stereotype.Service;

import com.avatarcode.orderservice.dto.OrderLineitemsDto;
import com.avatarcode.orderservice.dto.OrderRequest;
import com.avatarcode.orderservice.model.Order;
import com.avatarcode.orderservice.model.OrderLinesItems;
import com.avatarcode.orderservice.repository.OrderRepository;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@RequiredArgsConstructor
@Slf4j
public class OrderService {
    
    private final OrderRepository orderRepository;

    public void placeOrder(OrderRequest orderRequest){
       Order order = new Order();
       order.setOderNumber(UUID.randomUUID().toString());

       List<OrderLinesItems> orderLinesItems = orderRequest.getOrderLineitemsDtoLidt()
       .stream()
       .map(orderLineDto-> mapToDto(orderLineDto)).toList();

       order.setOderLineitemsList(orderLinesItems);
       System.err.println("valeur ------> "+orderLinesItems.get(0));

       orderRepository.save(order);

       log.info("order create avec succès {}"+order.getId());

    }

    public  OrderLinesItems mapToDto (OrderLineitemsDto orderLineitemsDto){
           return OrderLinesItems.builder()
                  .price(orderLineitemsDto.getPrice())
                  .quandite(orderLineitemsDto.getQuandite())
                  .skuCode(orderLineitemsDto.getSkuCode())
                  .build();
    }

}
