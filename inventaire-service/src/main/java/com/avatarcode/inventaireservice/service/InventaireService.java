package com.avatarcode.inventaireservice.service;

import com.avatarcode.inventaireservice.repository.InventaireRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class InventaireService {

    private final InventaireRepository inventaireRepository;


    
    public boolean isInStock(String skuCode){
         return   inventaireRepository.findBySkuCode(skuCode).isPresent();
    }
}
