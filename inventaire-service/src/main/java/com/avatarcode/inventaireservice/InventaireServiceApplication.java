package com.avatarcode.inventaireservice;

import com.avatarcode.inventaireservice.model.Inventaire;
import com.avatarcode.inventaireservice.repository.InventaireRepository;
import com.avatarcode.inventaireservice.service.InventaireService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class InventaireServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(InventaireServiceApplication.class, args);
	}


	@Bean
	public CommandLineRunner loadData(InventaireRepository inventaireRepository){
		return args -> {
			Inventaire inventaire = new Inventaire();
			inventaire.setSkuCode("iphone 13");
			inventaire.setQuadite(20);
			
			
			Inventaire inventaire1 = new Inventaire();
			inventaire.setSkuCode("iphone 13-12");
			inventaire.setQuadite(0);
			
			inventaireRepository.save(inventaire);
			inventaireRepository.save(inventaire1);
		};
	}
}
