package com.avatarcode.inventaireservice.repository;

import com.avatarcode.inventaireservice.model.Inventaire;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface InventaireRepository  extends JpaRepository<Inventaire , Long> {

    @Query(value = "select i from Inventaire i where i.skuCode =?1")
     public Optional<Inventaire> findBySkuCode(String skuCode );

}
